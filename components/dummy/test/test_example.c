#include "dummy.h"
#include "unity.h"

TEST_CASE("Test MAX function 1", "[tag0]")
{
  int arr[] = {-1, -10, -8, -4, -100, 5, 2, 3, 4, 1};

  TEST_ASSERT_EQUAL_INT(max(arr, 5), -1);
  TEST_ASSERT_EQUAL_INT(max(arr, 10), 5);
}

TEST_CASE("Another test", "[tag1]")
{
  int arr[] = {-1, -10, -8, -4, -100, 5, 2, 3, 4, 1};

  TEST_ASSERT_EQUAL_INT(max(arr, 5), -1);
  TEST_ASSERT_EQUAL_INT(max(arr, 10), 5);
}
