#include "dummy.h"

int max(int *arr, size_t size) {
  int temp = arr[0];
  for (size_t i = 0; i < size; i++) {
    if (temp < arr[i]) {
      temp = arr[i];
    }
  }
  
  return temp;
}