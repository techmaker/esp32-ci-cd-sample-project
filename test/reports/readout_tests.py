import serial

ser = serial.Serial()
ser.baudrate = 115200
ser.port = '/dev/ttyUSB0'
ser.timeout = 1
ser.open()
ser.write(b'y')
line = b''
f = open("report.test", "w")

while b'REPORT_START' not in line:
    line = ser.readline()

line = ser.readline()
while b'REPORT_END' not in line:
    f.write(line.decode('ascii', errors='ignore'))
    line = ser.readline()

f.close()
ser.close()