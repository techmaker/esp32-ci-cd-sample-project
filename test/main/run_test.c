#include <stdio.h>
#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "unity.h"

static void print_banner(const char* text);

void app_main(void)
{
    while (1) {
        while ('y' != fgetc(stdin)); // wait for user input
        print_banner("REPORT_START");
        UNITY_BEGIN();
        unity_run_all_tests();
        UNITY_END();
        print_banner("REPORT_END");
        vTaskDelay(5);
    }
}


static void print_banner(const char* text)
{
    printf("\n##### %s #####\n\n", text);
}
